// Rddvl reads data records from the Doppler Velocity Log publishes them
// to a NATS Streaming Server.
package main

import (
	"bytes"
	"context"
	"encoding/binary"
	"encoding/json"
	"flag"
	"fmt"
	"log"
	"os"
	"os/signal"
	"runtime"
	"syscall"
	"time"

	dvl "bitbucket.org/uwaploe/go-dvl"
	stan "github.com/nats-io/go-nats-streaming"
	"github.com/pkg/errors"
	"github.com/vmihailenco/msgpack"
)

const Usage = `Usage: rddvl [options]

Read data from the MuST DVL and publish to a NATS Streaming Server.
`

var Version = "dev"
var BuildDate = "unknown"

var (
	showVers = flag.Bool("version", false,
		"Show program version information and exit")
	natsURL     string = "nats://localhost:4222"
	clusterID   string = "must-cluster"
	dvlHost     string
	dvlSubject  string
	rawSubject  string
	timeout     time.Duration = time.Second * 5
	dataTimeout time.Duration
	qLen        int = 4
)

func sensorInit(sensor *dvl.RdiDvl, cmds, tracefile string) error {
	err := sensor.Wakeup()
	if err != nil {
		return errors.Wrap(err, "DVL wakeup")
	}

	err = sensor.SyncClock(true)
	if err != nil {
		return errors.Wrap(err, "sync clock")
	}

	file, err := os.Open(cmds)
	if err != nil {
		return errors.Wrap(err, "open command file")
	}
	defer file.Close()
	trace, err := sensor.Upload(file)
	if tracefile != "" {
		f, err := os.Create(tracefile)
		if err != nil {
			return errors.Wrap(err, "open trace file")
		}
		defer f.Close()
		enc := json.NewEncoder(f)
		err = enc.Encode(trace)
	}

	return err
}

func parseCmdLine() []string {
	flag.Usage = func() {
		fmt.Fprintf(os.Stderr, Usage)
		flag.PrintDefaults()
	}

	flag.StringVar(&natsURL, "nats-url", lookupEnvOrString("NATS_URL", natsURL),
		"URL for NATS Streaming Server")
	flag.StringVar(&clusterID, "cid", lookupEnvOrString("NATS_CLUSTER_ID", clusterID),
		"NATS cluster ID")
	flag.StringVar(&dvlHost, "dvl-host", lookupEnvOrString("DVL_HOST", dvlHost),
		"DVL hostname or IP address")
	flag.StringVar(&dvlSubject, "sub", lookupEnvOrString("DVL_SUBJECT", dvlSubject),
		"Subject name for DVL data")
	flag.StringVar(&rawSubject, "raw-sub", lookupEnvOrString("DVL_RAW_SUBJECT", rawSubject),
		"Subject name for raw DVL data")
	flag.DurationVar(&timeout, "timeout", lookupEnvOrDuration("DVL_TIMEOUT", timeout),
		"Timeout for DVL command response")
	flag.DurationVar(&dataTimeout, "data-timeout",
		lookupEnvOrDuration("DVL_DATA_TIMEOUT", dataTimeout),
		"Timeout for data record stream")
	flag.IntVar(&qLen, "qlen", lookupEnvOrInt("DVL_QLEN", qLen),
		"Size of data record queue")
	flag.Parse()

	if *showVers {
		fmt.Fprintf(os.Stderr, "%s %s\n", os.Args[0], Version)
		fmt.Fprintf(os.Stderr, "  Built with: %s\n", runtime.Version())
		os.Exit(0)
	}

	return flag.Args()
}

func main() {

	args := parseCmdLine()

	if dvlHost == "" {
		log.Fatal("No DVL device specified, aborting")
	}

	sc, err := stan.Connect(clusterID, "dvl-pub", stan.NatsURL(natsURL))
	if err != nil {
		log.Fatalf("Cannot connect: %v", err)
	}
	defer sc.Close()

	opts := []dvl.NetworkOption{
		dvl.ReadTimeout(timeout),
		dvl.Pd4Stream(fmt.Sprintf("%s:%d", dvlHost, dvl.Pd4Port))}
	sensor, err := dvl.Dial(fmt.Sprintf("%s:%d", dvlHost, dvl.CommandPort), opts...)
	if err != nil {
		log.Fatal(err)
	}

	switch len(args) {
	case 2:
		err = sensorInit(sensor, args[0], args[1])
	case 1:
		err = sensorInit(sensor, args[0], "")
	}
	if err != nil {
		log.Fatal(err)
	}

	// Initialize signal handler
	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM, syscall.SIGHUP)
	defer signal.Stop(sigs)

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	// Goroutine to wait for signals or for the signal channel
	// to close.
	go func() {
		s, more := <-sigs
		if more {
			log.Printf("Got signal: %v", s)
			cancel()
		}
	}()

	// Start a worker to unpack, Msgpack encode, and publish
	// the data records
	queue := make(chan []byte, qLen)
	defer close(queue)

	go func() {
		for b := range queue {
			if b[1] != dvl.Pd4DataId {
				continue
			}
			rec := dvl.PD4{}
			err := binary.Read(bytes.NewReader(b), dvl.ByteOrder, &rec)
			if err != nil {
				log.Printf("Record unpack failed: %v", err)
				continue
			}
			msg, err := msgpack.Marshal(&rec)
			if err != nil {
				log.Printf("Msgpack encoding failed: %v", err)
				continue
			}
			err = sc.Publish(dvlSubject, msg)
			if err != nil {
				log.Printf("Publish to %q failed: %v", dvlSubject, err)
			}
		}
	}()

	ch, err := sensor.StreamData(ctx, fmt.Sprintf("%s:%d", dvlHost, dvl.Pd4Port),
		dataTimeout)
	if err != nil {
		log.Fatalf("Cannot stream data: %v", err)
	}

	// Publish the raw data records
	for rec := range ch {
		err = sc.Publish(rawSubject, rec)
		if err != nil {
			log.Printf("Publish to %q failed: %v", rawSubject, err)
		}
		select {
		case queue <- rec:
		default:
			log.Println("WARNING: Encode queue full")
		}
	}
}
