# DVL Data Reader

Rddvl reads data records from the MUST Doppler Velocity Log and publishes them to a [NATS Streaming Server](https://nats.io/documentation/streaming/nats-streaming-intro/). The input records are in the "PD4" format as described in Chapter 8 of the *Pathfinder DVL Guide*.

## Data Format

The input record values are published in [Message Pack](https://msgpack.org/index.html) format as an associative array (map) with the following entries:

| **Name**  | **Type**        | **Description**                                    |
|:----------|:----------------|:---------------------------------------------------|
| HeaderId  | 1-byte integer  | Constant, hex 7d                                   |
| DataId    | 1-byte integer  | Constant, hex 00                                   |
| Nbytes    | 2-byte integer  | Record size (not including checksum)               |
| SysConfig | 1-byte integer  | System configuration bitmask                       |
| VelBtm    | 4-element array | Velocity relative to the bottom in mm/s            |
| RngBtm    | 4-element array | Range to bottom in cm                              |
| BtmStatus | 1-byte integer  | Bottom return bitmask                              |
| Vel       | 4-element array | Velocity relative to water reference layer in mm/s |
| RefStart  | 2-byte integer  | Reference layer start depth in dm                  |
| RefEnd    | 2-byte integer  | Reference layer stop depth in dm                   |
| RefStatus | 1-byte integer  | Reference layer return bitmask                     |
| T         | map             | Timestamp (see below)                              |
| BitResult | 2-byte integer  | BIT result bitmask                                 |
| SndSpeed  | 2-byte integer  | Sound speed in m/s                                 |
| Temp      | 2-byte integer  | Water temperature in degC/100                      |
| Checksum  | 2-byte integer  |                                                    |

Timestamp format

| **Name** | **Type**       | **Description**        |
|:---------|:---------------|:-----------------------|
| Hour     | 1-byte integer | Hour, 0-23             |
| Min      | 1-byte integer | Minute (0-59)          |
| Sec      | 1-byte integer | Second (0-59)          |
| Hsec     | 1-byte integer | Hundredths of a second |

## Environment Variables

The following environment variables affect the program operation.

| **Name**           | **Description**                      | **Required?** |
|:-------------------|:-------------------------------------|:--------------|
| NATS_URL           | URL of NATS Streaming Server         | yes           |
| NATS_CLUSTER_ID    | ID of NATS cluster                   | yes           |
| DVL_HOST           | IP address of the DVL                | yes           |
| DVL_SUBJECT        | Subject (channnel) for data messages | yes           |
| DVL_TIMEOUT        | DVL command timeout                  | yes           |
| DVL_STREAM_TIMEOUT | DVL data record timeout              | no            |
